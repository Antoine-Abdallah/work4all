# GitHub User Search App

## Description:

This a simple web application using GitHub REST Api for searching github users and creating a list of favorite users which can be edited as desired.

## Project Screen Shots

Search page :

![Search_Page](./Project%20pics/SearchPage.png)

Favorites page :

![Favorites_Page](./Project%20pics/FavoritesPage.png)

User page :

![User_Page](./Project%20pics/UserPage.png)

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.

Installation:

`npm install --force`

To Start Server:

`npm start`

To Test:

`npm test`

To Visit App:

`localhost:3000/`

## Reflection

- This was a 1 week long project built for work4all as a React js task. Project goals was build a web application that calls github's user search api and search for users by their user name.

- Originally I wanted to build an application that allowed users to pull data from the Github API based on what they were interested in. I started this process by using the `create-react-app` boilerplate, then adding the dependencies needed such as: -`react-router-dom`: "^6.3.0" . -`bootstrap`: "^5.2.0". -`mdb-react-ui-kit`: "^4.2.0". -`react-bootstrap`: "^2.5.0". -`saas`:^1.54.3".

- I ran into some challenges such as Authentication with GitHub Api (which a chose to authenticate using the PAT) and trying to make the project as responsive as possible which I implemented using `react-bootstrap`.

- At the end of the day, the technologies implemented in this project are React, React-Router-Dom 6.3.0, Bootstrap, LoDash, D3, and a significant amount of JSX, and saas. I chose to use the `create-react-app` boilerplate to minimize initial setup and invest more time in diving into weird technological rabbit holes.
