import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useEffect, useLayoutEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./assets/sass/global.scss";
import routes from "./components/gloabl/routes";

import Search from "./pages/Search";
import Favorites from "./pages/Favorites";
import User from "./pages/User";
import Nav from "./components/Navbar";

function App() {
	const [name, setName] = useState("");
	const [trigger, setTrigger] = useState(false);
	const changeName = (user) => {
		setName(user);
		console.log(name);
	};
	return (
		<div className="App" data-testid="myApp">
			{/* for the Toast */}
			<ToastContainer
				position="bottom-left"
				autoClose={5000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
			/>
			<header>
				<Nav user={name} setUser={setName} setSearch={setTrigger} />
			</header>
			{/* Start pages */}
			<main>
				<Routes>
					<Route
						exact
						path="/"
						element={
							<Search user={name} doSearch={trigger} setDoSearch={setTrigger} />
						}
					/>
					<Route exact path="/Favorites" element={<Favorites />} />
					<Route exact path="/User/:name/:isFav" element={<User />} />
				</Routes>
			</main>
		</div>
	);
}

export default App;
