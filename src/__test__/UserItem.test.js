import { render, screen } from "@testing-library/react";
import UserItem from "../UserItem";

test("renders learn react link", () => {
	const user = {
		avatar_url: "https://avatars.githubusercontent.com/u/25642743?v=4",
		events_url: "https://api.github.com/users/fdtindustries/events{/privacy}",
		followers_url: "https://api.github.com/users/fdtindustries/followers",
		following_url:
			"https://api.github.com/users/fdtindustries/following{/other_user}",
		gists_url: "https://api.github.com/users/fdtindustries/gists{/gist_id}",
		gravatar_id: "",
		html_url: "https://github.com/fdtindustries",
		id: 25642743,
		login: "fdtindustries",
		node_id: "MDQ6VXNlcjI1NjQyNzQz",
		organizations_url: "https://api.github.com/users/fdtindustries/orgs",
		received_events_url:
			"https://api.github.com/users/fdtindustries/received_events",
		repos_url: "https://api.github.com/users/fdtindustries/repos",
		score: 1,
		site_admin: false,
		starred_url:
			"https://api.github.com/users/fdtindustries/starred{/owner}{/repo}",
		subscriptions_url:
			"https://api.github.com/users/fdtindustries/subscriptions",
		type: "User",
		url: "https://api.github.com/users/fdtindustries",
	};
	const { getByTestId } = render(
		<UserItem
			Item={user}
			handelFav={() => handelAdd(user)}
			navigateToUser={() => handelNavigation(user)}
			isFav={1}
			key={user.id}
		/>
	);

	expect(getByTestId("user-name")).toHaveTextContent("fdtindustries");
});
