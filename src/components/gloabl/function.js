import { ToastContainer, toast } from "react-toastify";

//calling base URL from env File

export const BASE_URL = process.env.REACT_APP_API_URL;
export const MY_TOKEN = process.env.REACT_APP_GITHUB_TOKEN;

//toast functions

export function info(message) {
	toast(message);
}

export function warning(message) {
	toast.warning(message);
}

export function error(message) {
	toast.error(message);
}

export function success(message) {
	toast.success(message);
}

//API handler

export function handleAPI(t, navigate, errorCode) {
	switch (errorCode) {
		case 8:
			error("somthing wrong");

			break;
		case 10:
			error("somthing wrong");

			break;
		case 11:
			error("some data missing");

			break;
		case 12:
			error("wrong data entered");

			break;
		case 13:
			error("you need to login again");
			setTimeout(() => {
				navigate("/LOGIN");
			}, 4000);
			break;
		case 14:
			error("you internet connection slow");

			break;
		case 15:
			error("somthing wrong");

			break;
		case 16:
			error("somthing wrong");

			break;
		case 17:
			error("somthing wrong");

			break;
		case 18:
			error("somthing wrong");

			break;
		case 19:
			error("please enter a valid date");

			break;
		case 20:
			error("Device not registered");

			break;
		case 21:
			error("Device Inactive");

			break;
		case 22:
			error("Country Not Supported");

			break;
		case 23:
			error("PIN Expired");

			break;
		case 24:
			error("VerificationExpired");

			break;
		case 25:
			error("InvalidPIN ");

			break;
		case 26:
			error("InvalidEmail");

			break;
		case 27:
			navigate("/Zerogems");
			break;
		case 30:
			error("ClientAlreadyExists");

			break;
		case 31:
			error("ClientInactive ");

			break;
		case 32:
			error("ClientNotVerified");

			break;
		case 41:
			error("InvalidCallee");

			break;
		case 42:
			error("CalleeBusy ");

			break;
		case 43:
			error("CalleeOffline");

			break;
		case 44:
			error("BroadcastDurationTimeout ");

			break;
		case 75:
			error("InvalidContentType ");

			break;
		default:
			error(
				t(
					"Some Thing went Wrong Please Check Your Internet Connection and Make Sure that you Fill Missing Data"
				)
			);
	}
}
