import Search from "../../pages/Search";
import Favorites from "../../pages/Favorites";
import User from "../../pages/User";

const routes = [
	{
		type: "collapse",
		name: "Home",
		key: "Home",
		//icon: <Icon fontSize='small'>Home</Icon>,
		route: "/",
		component: <Search />,
	},
	{
		type: "collapse",
		name: "Favorites",
		key: "Favorites",
		//icon: <Icon fontSize='small'>Login</Icon>,
		route: "/Favorites",
		component: <Favorites />,
	},
	{
		type: "collapse",
		name: "User",
		key: "User",
		//icon: <Icon fontSize='small'>Login</Icon>,
		route: "/User/:name/:isFav",
		component: <User />,
	},
];

export default routes;
