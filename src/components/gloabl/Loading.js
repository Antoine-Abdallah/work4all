import React from "react";

function Loading() {
	return (
		<div className="loading-div">
			<div className="loading">Loading&#8230;</div>
		</div>
	);
}

export default Loading;
