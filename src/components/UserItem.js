import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { MDBListGroupItem } from "mdb-react-ui-kit";

import star from "../assets/img/star.svg";
import yellowStar from "../assets/img/yellowstar.svg";

function UserItem(props) {
	const [user, setUser] = useState({});
	const [isFav, setIsFav] = useState(props.isFav);
	const handelFav = (user) => {
		props.handelFav(user);
		setIsFav(!isFav);
	};
	const navigateToUser = (user) => props.navigateToUser(user);

	useEffect(() => {
		setUser(props.Item);
	}, [props.Item]);

	return (
		<MDBListGroupItem className="users-list-item">
			<Row>
				<Col xs="2">
					<div className="user-avatar">
						<img className="user-avatar-image" src={user.avatar_url} />
					</div>
				</Col>
				<Col
					xs="8"
					md="9"
					className="text-start"
					onClick={() => navigateToUser(user)}
					style={{ cursor: "pointer" }}
				>
					<p data-testid="user-name">{user.login}</p>
				</Col>
				<Col xs="2" md="1" className="m-auto" onClick={() => handelFav(user)}>
					<div className="m-auto" style={{ cursor: "pointer" }}>
						<img className="m-auto" src={isFav == 1 ? yellowStar : star} />
					</div>
				</Col>
			</Row>
		</MDBListGroupItem>
	);
}

export default UserItem;
