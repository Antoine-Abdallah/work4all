import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import ListGroup from 'react-bootstrap/ListGroup'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import Logo from '../assest/img/Logo On Light BG.png'
function Footer() {
  const [t, i18n] = useTranslation()
  return (
    <div className='footer'>
      <Container className='w-100'>
        <Row className='w-100 footer-m-auto'>
          <Col xs='12' md='3'>
            <div
              className={
                sessionStorage.getItem('lang') == 'en'
                  ? 'footer-image '
                  : 'footer-image image-flip m-auto'
              }
            >
              <img src={Logo} alt='Footer IMG' />
            </div>
          </Col>
          <Col xs='12' md='3' className='footer-text footer-link'>
            <ListGroup variant='flush'>
              <ListGroup.Item>
                <p
                  className='text-center color-white'
                  style={{ fontSize: '22px' }}
                >
                  bar
                  <img
                    src='https://via.placeholder.com/30*30?text=HNNDES'
                    className='d-block m-auto w-30'
                    alt=''
                    srcset=''
                  />
                </p>
              </ListGroup.Item>
              <ListGroup.Item>
                <p
                  className='text-center color-white '
                  style={{ fontSize: '22px' }}
                >
                  bar
                  <div className='m-auto w-20'>
                    <img
                      src='https://via.placeholder.com/90*30?text=HNNDES'
                      className='d-block m-auto w-90'
                      alt=''
                      srcset=''
                    />
                  </div>
                </p>
              </ListGroup.Item>
            </ListGroup>
          </Col>
          <Col xs='12' md='3' className='footer-text footer-link'>
            <ListGroup variant='flush'>
              <ListGroup.Item>
                <p
                  className='text-center color-white'
                  style={{ fontSize: '22px' }}
                >
                  bar
                  <img
                    src='https://via.placeholder.com/fff/30*30?text=HNNDES'
                    className='d-block m-auto w-30'
                    alt=''
                    srcset=''
                  />
                </p>
              </ListGroup.Item>
            </ListGroup>
          </Col>
          <Col xs='12' md='3' className='footer-text footer-link'>
            <ListGroup variant='flush'>
              <ListGroup.Item>
                <p className='text-center color-white'>
                  <span style={{ fontSize: '22px' }}>bar</span>
                  <p style={{ color: '#fff', fontSize: '14px' }}>
                    Email: info@oldtimer-em.com
                  </p>
                  <p style={{ color: '#fff', fontSize: '14px' }}>
                    Location: Unit7, Business Park1,
                  </p>
                  <p style={{ color: '#fff', fontSize: '14px' }}>
                    phone: +971 56 480 4689
                  </p>
                </p>
              </ListGroup.Item>
            </ListGroup>
          </Col>
        </Row>
        <Row style={{ borderTop: '.5px solid white' }}>
          <Col>
            <p
              className='text-end'
              style={{ fontSize: '10px', color: 'white' }}
            >
              All rights reserved for &copy; HNNDES.co
            </p>
          </Col>
          <Col xs='12' md='6'>
            <p
              className='text-start'
              style={{ fontSize: '10px', color: 'white' }}
            ></p>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Footer
