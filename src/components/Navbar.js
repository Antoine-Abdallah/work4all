import React, { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";

import Navbar from "react-bootstrap/Navbar";
import { Row, Col, Form } from "react-bootstrap";

import star from "../assets/img/star.svg";
import yellowStar from "../assets/img/yellowstar.svg";
import search from "../assets/img/search.svg";
import back from "../assets/img/back.svg";

export default function Nav(props) {
	let location = useLocation();
	let navigate = useNavigate();
	let params = useParams();

	const [url, setUrl] = useState("");

	useEffect(() => {
		setUrl(location.pathname);
	}, [location]);

	const navigateToFavorites = () => {
		navigate("/Favorites");
	};

	const handelSearch = () => {
		props.setSearch(true);
	};

	if (url == "/") {
		return (
			<div className="search">
				<div className="m-auto search-div">
					<Navbar className="search-nav">
						<div className="search-Row m-auto">
							<Row className="m-auto w-100">
								<Col xs="1" className="m-start">
									<div
										className="m-start search-img-div"
										onClick={handelSearch}
									>
										<img className="search-img" src={search} />
									</div>
								</Col>
								<Col xs="10">
									<Form.Control
										className="search-input"
										type="string"
										onChange={(e) => props.setUser(e.target.value)}
										placeholder="Search for Github users..."
									/>
								</Col>
								<Col xs="1" className="m-auto search-star-col">
									<div className="m-auto" onClick={navigateToFavorites}>
										<img className="m-auto" src={star} />
									</div>
								</Col>
							</Row>
						</div>
					</Navbar>
				</div>
			</div>
		);
	} else if (url == "/Favorites") {
		return (
			<div className="favorite">
				<div className="m-auto ">
					<Navbar className="search-nav">
						<div className="search-Row m-auto">
							<Row className="m-auto w-100">
								<Col xs="11" className="d-flex">
									<span className="">
										<div
											className=" search-img-div"
											onClick={() => navigate(-1)}
										>
											<img className="search-img" src={back} />
										</div>
									</span>
									<p className="search-input favorite-p text-start">
										Favorites
									</p>
								</Col>
								<Col xs="1" className="m-auto search-star-col">
									<div className="m-auto">
										<img className="m-auto" src={yellowStar} />
									</div>
								</Col>
							</Row>
						</div>
					</Navbar>
				</div>
			</div>
		);
	} else if (url.includes("/User/")) {
		let user = JSON.parse(sessionStorage.getItem("user"));
		return (
			<div className="m-auto search-div user">
				<Navbar className="search-nav">
					<div className="search-Row m-auto">
						<Row className="m-auto w-100">
							<Col xs="11" className="d-flex">
								<span className="">
									<div className=" search-img-div" onClick={() => navigate(-1)}>
										<img className="search-img" src={back} />
									</div>
								</span>
								<p className="search-input favorite-p text-start">
									{user.login}
								</p>
							</Col>
						</Row>
					</div>
				</Navbar>
			</div>
		);
	}
}
