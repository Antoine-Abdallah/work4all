import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Search from "../Search";

const MockSearch = () => {
	return (
		<BrowserRouter>
			<Search user="als" />
		</BrowserRouter>
	);
};

describe("Search using api", () => {
	test("renders learn react link", async () => {
		render(<MockSearch />);
		setTimeout(async () => {
			const userElment = await screen.findByTestId("search-result-1");

			expect(userElment).toBeInTheDocument();
		}, 3000);
	});
});
