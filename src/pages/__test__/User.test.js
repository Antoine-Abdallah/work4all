import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import User from "../User";

const user = [
	{
		avatar_url: "https://avatars.githubusercontent.com/u/25642743?v=4",
		events_url: "https://api.github.com/users/fdtindustries/events{/privacy}",
		followers_url: "https://api.github.com/users/fdtindustries/followers",
		following_url:
			"https://api.github.com/users/fdtindustries/following{/other_user}",
		gists_url: "https://api.github.com/users/fdtindustries/gists{/gist_id}",
		gravatar_id: "",
		html_url: "https://github.com/fdtindustries",
		id: 25642743,
		login: "fdtindustries",
		node_id: "MDQ6VXNlcjI1NjQyNzQz",
		organizations_url: "https://api.github.com/users/fdtindustries/orgs",
		received_events_url:
			"https://api.github.com/users/fdtindustries/received_events",
		repos_url: "https://api.github.com/users/fdtindustries/repos",
		score: 1,
		site_admin: false,
		starred_url:
			"https://api.github.com/users/fdtindustries/starred{/owner}{/repo}",
		subscriptions_url:
			"https://api.github.com/users/fdtindustries/subscriptions",
		type: "User",
		url: "https://api.github.com/users/fdtindustries",
	},
];

sessionStorage.setItem("user", JSON.stringify(user));

const MockUser = () => {
	return (
		<BrowserRouter>
			<User />
		</BrowserRouter>
	);
};

describe("user page", () => {
	test("fetch user data from the api", async () => {
		render(<MockUser />);
		setTimeout(async () => {
			const userElment = await screen.findByTestId("user-link");
			expect(userElment).toHaveTextContent("fdtindustries");
		}, 3000);
	});
});
