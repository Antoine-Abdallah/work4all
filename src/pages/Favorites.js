import React, { useState, useEffect } from "react";
import { Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { MDBListGroup, MDBListGroupItem } from "mdb-react-ui-kit";
import { Button, Form, FormGroup, Row, Col } from "react-bootstrap";

import UserItem from "../components/UserItem";

import star from "../assets/img/star.svg";
import divider from "../assets/img/Divider.svg";
import yellowStar from "../assets/img/yellowstar.svg";
import back from "../assets/img/back.svg";

function Favorites() {
	let navigate = useNavigate();

	const [users, setUsers] = useState([]);
	const [removed, setRemoved] = useState(1);
	const [show, setShow] = useState(true);
	var myList = [];
	if (sessionStorage.getItem("favorites")) {
		myList = JSON.parse(sessionStorage.getItem("favorites"));
	}

	const handelClick = (user) => {
		const userIndex = myList.findIndex(
			(element) => element.login == user.login
		);
		myList = myList.filter((element) => element.login !== user.login);
		sessionStorage.setItem("favorites", JSON.stringify(myList));
		setRemoved(!removed);
		if (myList.length == 0) {
			setShow(false);
		}
	};

	const handelNavigation = (user) => {
		sessionStorage.setItem("user", JSON.stringify(user));
		navigate(`/User/${user.login}/1`);
	};
	return (
		<div className="favorite">
			<div className="m-auto search-div">
				{myList.length != 0 ? (
					<div className="users-list" data-testid="user-fav">
						{myList.map((user, index) => {
							return (
								<>
									<MDBListGroup key={index} className="mt-1">
										<UserItem
											data-testid="user-fav"
											Item={user}
											handelFav={() => handelClick(user)}
											navigateToUser={() => handelNavigation(user)}
											isFav={1}
											key={user.id}
										/>
										<div className="m-auto">
											<img src={divider} />
										</div>
									</MDBListGroup>
								</>
							);
						})}
					</div>
				) : (
					<p className="no-fav" data-testid="user-no-fav">
						You dont have any favorite users yet...
					</p>
				)}
			</div>
		</div>
	);
}

export default Favorites;
