import React, { useEffect, useState } from "react";
import { Button, Form, FormGroup, Row, Col } from "react-bootstrap";
import { Navbar } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { error, success } from "../components/gloabl/function";

import Loading from "../components/gloabl/Loading";
import { BASE_URL, MY_TOKEN } from "../components/gloabl/function";

import star from "../assets/img/star.svg";
import yellowStar from "../assets/img/yellowstar.svg";
import back from "../assets/img/back.svg";

function User() {
	let navigate = useNavigate();
	let params = useParams();

	var myList = [];
	if (sessionStorage.getItem("favorites")) {
		myList = JSON.parse(sessionStorage.getItem("favorites"));
	}

	const [user, setUser] = useState(JSON.parse(sessionStorage.getItem("user")));
	const [followers, setFollowers] = useState(null);
	const [following, setFollowing] = useState(null);
	const [repos, setRepos] = useState(null);
	const [loading, setLoading] = useState(true);
	const [favorite, setFavorite] = useState(params.isFav);
	const [render, setRender] = useState(1);
	const [page, setPage] = useState(1);
	const [pageFollowing, setPageFollowing] = useState(1);
	const [pageRepo, setPageRepo] = useState(1);

	const [follower, setFollower] = useState(false);
	const [followings, setFollowings] = useState(false);
	const [repo, setRepo] = useState(false);

	const headers = {
		Accept: "application/vnd.github.cloak-preview",
		Authorization: `Token ${MY_TOKEN}`,
	};

	//user Info fetch function

	const userInfo = () => {
		fetch(user.followers_url + "?page=2", {
			method: "GET",
			headers: headers,
		})
			.then((res) => res.json())
			.then((data) => {
				// setFollowers(data.length);
				// console.log(data);
			})
			.then(() => {
				fetch(BASE_URL + `users/${user.login}/following`, {
					method: "GET",
					headers: headers,
				})
					.then((res) => res.json())
					.then((data) => {
						setFollowing(data.length);
					})
					.then(() => {
						fetch(user.repos_url, {
							method: "GET",
							headers: headers,
						})
							.then((res) => res.json())
							.then((data) => {
								setLoading(false);
								setRepos(data.length);
							})
							.catch((err) => error(err));
					})
					.catch((err) => error(err));
			})
			.catch((err) => error(err));
	};

	//fetching the user info for once
	useEffect(() => {
		if (follower && followings && repo) {
			setLoading(false);
		}
	}, [follower, followings, repo]);

	//user's followers
	useEffect(() => {
		fetch(user.followers_url + `?page=${page}&per_page=100`, {
			method: "GET",
			headers: headers,
		})
			.then((res) => res.json())
			.then((data) => {
				if (followers + data.length == followers) {
					setFollower(true);
					return;
				} else {
					setFollowers(followers + data.length);
					setPage(page + 1);
				}
			});
	}, [page]);

	//user's following
	useEffect(() => {
		fetch(
			BASE_URL +
				`users/${user.login}/following?page=${pageFollowing}&per_page=100`,
			{
				method: "GET",
				headers: headers,
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (following + data.length == following) {
					setFollowings(true);
					return;
				} else {
					setFollowing(following + data.length);
					setPageFollowing(pageFollowing + 1);
				}
			});
	}, [pageFollowing]);

	//user's repos
	useEffect(() => {
		fetch(user.repos_url + `?page=${pageRepo}&per_page=100`, {
			method: "GET",
			headers: headers,
		})
			.then((res) => res.json())
			.then((data) => {
				if (repos + data.length == repos) {
					setRepo(true);
					return;
				} else {
					setRepos(repos + data.length);
					setPageRepo(pageRepo + 1);
				}
			});
	}, [pageRepo]);

	//handeling add and remove to favorites list
	const handelFav = (user) => {
		const index = myList.findIndex((element) => element.id == user.id);
		if (index == -1) {
			myList.push(user);
			sessionStorage.setItem("favorites", JSON.stringify(myList));
			setFavorite(1);
		} else {
			myList = myList.filter((element) => element.id !== user.id);
			sessionStorage.setItem("favorites", JSON.stringify(myList));
			setFavorite(0);
		}
	};
	if (loading) {
		return <Loading />;
	}

	return (
		<div className="m-auto search-div user">
			<div className="search-div">
				<div className="search-div-user">
					<div className="user-div">
						<Row>
							<Col xs="12" sm="4">
								<div className="avatar-div">
									<img className="avatar-img" src={user.avatar_url} />
								</div>
							</Col>
							<Col>
								<Row>
									<Col className="user-name">
										<p className="user-name-p">{user.login}</p>
										<a
											href={user.html_url}
											className="user-link"
											data-testid="user-link"
										>
											{user.login}
										</a>
										<Row className="user-info-Row">
											<Col xs="4" className="user-info-col">
												<p className="info-num">{followers}</p>
												<p className="info-title">FOLLOWERS</p>
											</Col>
											<Col xs="4" className="user-info-col">
												<p className="info-num">{following}</p>
												<p className="info-title">FOLLOWING</p>
											</Col>
											<Col xs="4" className="user-info-col">
												<p className="info-num">{repos}</p>
												<p className="info-title">REPOS</p>
											</Col>
										</Row>
									</Col>
									<Col xs="12" sm="2">
										<div className="star" onClick={() => handelFav(user)}>
											<img src={favorite == 1 ? yellowStar : star} />
										</div>
									</Col>
								</Row>
							</Col>
						</Row>
					</div>
				</div>
			</div>
		</div>
	);
}

export default User;
