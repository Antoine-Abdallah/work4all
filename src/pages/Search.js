import React, { useEffect, useState, useRef } from "react";
import { Navbar } from "react-bootstrap";
import { MDBListGroup, MDBListGroupItem } from "mdb-react-ui-kit";
import { MdOutlineSearch } from "react-icons/md";
import { useNavigate } from "react-router-dom";

import Loading from "../components/gloabl/Loading";
import UserItem from "../components/UserItem";

import { BASE_URL } from "../components/gloabl/function";
import { MY_TOKEN } from "../components/gloabl/function";

import divider from "../assets/img/Divider.svg";

function Search(props) {
	let navigate = useNavigate();
	const listRef = useRef();

	const [render, setRender] = useState(false);
	const [responses, setResponses] = useState([]);
	const [user, setUser] = useState("");
	const [show, setShow] = useState(false);
	const [loading, setLoading] = useState(false);
	const [page, setPage] = useState(1);

	//My favorite users's list
	var myList = [];
	if (sessionStorage.getItem("favorites")) {
		myList = JSON.parse(sessionStorage.getItem("favorites"));
	}

	//Api headers
	const headers = {
		Accept: "application/vnd.github.cloak-preview",
		Authorization: `Token ${MY_TOKEN}`,
	};

	//setting the user props
	useEffect(() => {
		setUser(props.user);
	}, [props.user]);

	//Search api function
	const handelSearch = () => {
		setLoading(true);
		setPage(page + 1);
		fetch(BASE_URL + `search/users?q=${user} in:name &page=${page}`, {
			method: "GET",
			headers: {
				Accept: "application/vnd.github.cloak-preview",
				Authorization: `Token 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`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (page > 1) {
					setResponses([...responses, ...data.items]);
				} else {
					setResponses(data.items);
				}
				setShow(true);
			})
			.then(() => {
				setLoading(false);
			})
			.catch((err) => console.log(err));
	};

	//Requesting search api when the name changes
	useEffect(() => {
		if (user.length >= 1) {
			setLoading(true);
			setPage(1);
			fetch(
				`https://api.github.com/search/users?q=${user} in:name &page=${page}`,
				{
					method: "GET",
					headers: headers,
				}
			)
				.then((res) => res.json())
				.then((data) => {
					setResponses(data.items);
					if (data.items == []) {
						setShow(false);
						console.log(show);
					} else {
						setShow(true);
					}
				})
				.then(() => {
					setLoading(false);
				})
				.catch((err) => console.log(err));
		}
	}, [user]);

	//searching on clicking the search icon
	useEffect(() => {
		if (props.doSearch == true) {
			handelSearch();
		}
	}, [props.doSearch]);

	//pagenation
	const handelScroll = () => {
		if (listRef.current) {
			const { scrollTop, scrollHeight, clientHeight } = listRef.current;
			if (scrollTop + clientHeight === scrollHeight && loading == false) {
				// This will be triggered after hitting the last element.
				// API call should be made here while implementing pagination.
				handelSearch();
			}
		}
	};

	//adding user to my favorite list
	const handelAdd = (user) => {
		const index = myList.findIndex((element) => element.id == user.id);
		if (index == -1) {
			myList.push(user);
			sessionStorage.setItem("favorites", JSON.stringify(myList));
		} else {
			myList = myList.filter((element) => element.id !== user.id);
			sessionStorage.setItem("favorites", JSON.stringify(myList));
		}
		setRender(!render);
	};

	//Navigating to the user's page
	const handelNavigation = (user) => {
		sessionStorage.setItem("user", JSON.stringify(user));
		if (myList.find((element) => element.id == user.id)) {
			navigate(`/User/${user.login}/1`);
		} else {
			navigate(`/User/${user.login}/0`);
		}
	};

	//set the screen width variable
	var root = document.documentElement;
	var width = (81 * window.innerWidth) / 100;
	root.style.setProperty("--screenWidth", width + "px");

	return (
		<div className="search">
			<div className="m-auto search-div">
				<div className="m-auto search-div-page">
					{loading ? <Loading /> : null}
					{console.log(responses)}
					{show === true && responses.length != 0 ? (
						<div className="users-list" ref={listRef} onScroll={handelScroll}>
							<MDBListGroup>
								{responses.map((user, index) => {
									let myOne = 0;
									if (myList != []) {
										if (myList.find((element) => element.id == user.id)) {
											myOne = 1;
										} else {
											myOne = 0;
										}
									}

									return (
										<>
											<UserItem
												data-testid={`search-result-${index}`}
												Item={user}
												handelFav={() => handelAdd(user)}
												navigateToUser={() => handelNavigation(user)}
												isFav={myOne}
												favList={myList}
												key={user.id}
											/>
											<div className="m-auto">
												<img src={divider} />
											</div>
										</>
									);
								})}
							</MDBListGroup>
						</div>
					) : (
						<p className="results">No search results...</p>
					)}
				</div>
			</div>
		</div>
	);
}

export default Search;
